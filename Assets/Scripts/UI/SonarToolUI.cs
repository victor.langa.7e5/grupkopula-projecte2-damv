using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SonarToolUI : ToolsUI
{
    [SerializeField] ToolSonar _playerSonar;
    [SerializeField] private Slider _cooldownSlider;

    public override void UpdateDisplay()
    {
        StartCoroutine(UpdateSonarCooldown());
    }


    private IEnumerator UpdateSonarCooldown()
    {
        _cooldownSlider.value = _playerSonar.CurrentCooldown / _playerSonar.Cooldown;

        yield return null;
        if (_playerSonar.CurrentCooldown < _playerSonar.Cooldown)
            StartCoroutine(UpdateSonarCooldown());
    }
}
