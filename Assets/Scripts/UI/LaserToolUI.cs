using UnityEngine;
using UnityEngine.UI;

public class LaserToolUI : ToolsUI
{
    [SerializeField] ToolLaser _playerLaser;
    [SerializeField] private Image _onOffImage;
    public override void UpdateDisplay()
    {
        UpdateOnOfIcon();
    }

    private void UpdateOnOfIcon()
    {
        if (_playerLaser.Light.enabled)
            _onOffImage.enabled = true;
        else
            _onOffImage.enabled = false;
    }
}
