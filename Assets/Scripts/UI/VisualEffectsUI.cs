using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class VisualEffectsUI : MonoBehaviour
{
    [SerializeField] RectTransform _playerUICanvas;
    Vector3 _canvasOriginalPosition;
    [SerializeField] RectTransform[] _playerElementsUI;
    [Space]
    [SerializeField] Vector2 _timeRange;

    private void Start()
    {
        _canvasOriginalPosition = _playerUICanvas.localPosition;
    }
    public void ResetUIEffect()
    {
        StopAllCoroutines();
        _playerUICanvas.localPosition = _canvasOriginalPosition;

        foreach (RectTransform _playerElementUI in _playerElementsUI)
        {
            _playerElementUI.gameObject.SetActive(true);
        }
    }

    public void DisorientedEffect()
    {
        for (int i = 0; i < 3; i++)
            StartCoroutine(DisableElementDuringTime());
    }

    private IEnumerator DisableElementDuringTime()
    {
        GameObject randomPlayerElementUI = _playerElementsUI[Random.Range(0, _playerElementsUI.Length)].gameObject;
        randomPlayerElementUI.SetActive(!randomPlayerElementUI.activeSelf);
        yield return new WaitForSeconds(Random.Range(_timeRange.x, _timeRange.y));
        randomPlayerElementUI.SetActive(!randomPlayerElementUI.activeSelf);
        StartCoroutine(DisableElementDuringTime());
    }

    public void ShakeUIEffect()
    {
        StartCoroutine(ShakeUIDuringTime());
    }

    private IEnumerator ShakeUIDuringTime()
    {
        _playerUICanvas.position = _canvasOriginalPosition + Random.insideUnitSphere * 10f;
        yield return null;
        StartCoroutine(ShakeUIDuringTime());
    }
}
