using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class LanternToolUI : ToolsUI
{
    [SerializeField] ToolLantern _playerLantern;
    [SerializeField] private Image _onOffImage;
    [SerializeField] private Slider _batterySlider;
    

    public override void UpdateDisplay()
    {
        UpdateOnOfIcon();
        StartCoroutine(UpdateBattery());
    }

    private IEnumerator UpdateBattery()
    {
        if (_batterySlider.enabled)
        {
            _batterySlider.value = _playerLantern.Charge / _playerLantern.MaxCharge;
    
            yield return null;
            if (_playerLantern.Light.enabled)
                StartCoroutine(UpdateBattery());
        }
    }

    private void UpdateOnOfIcon()
    {
        if (_playerLantern.Light.enabled)
            _onOffImage.enabled = true;
        else
            _onOffImage.enabled = false;
    }
}
