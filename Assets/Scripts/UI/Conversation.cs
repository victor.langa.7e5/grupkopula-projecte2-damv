using System.Collections;
using TMPro;
using UnityEngine;

public class Conversation : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _conversationText;
    [SerializeField] private float _timeBetweenLettersPrinted;

    [Space]
    [SerializeField] private Color _noneTextColor, _noahTextColor, _reneTextColor;

    private Coroutine _disableConversationCoroutine;

    public enum ESpeaker
    { 
        NONE,
        NOAH,
        REN�
    }

    public IEnumerator ShowSubtitle(string localizationKey, float timeBetweenLettersPrinted, ESpeaker speaker)
    {
        if (_disableConversationCoroutine != null)
            StopCoroutine(_disableConversationCoroutine);

        switch (speaker)
        {
            case ESpeaker.NONE:
                _conversationText.color = _noneTextColor;
                break;
            case ESpeaker.NOAH:
                _conversationText.color = _noahTextColor;
                break;
            case ESpeaker.REN�:
                _conversationText.color = _reneTextColor;
                break;
        }

        if (timeBetweenLettersPrinted == 0)
            timeBetweenLettersPrinted = _timeBetweenLettersPrinted;

        _conversationText.text = "";

        Localization.Instance.GetCurrentLanguageData().TryGetValue(localizationKey, out string finalValue);

        for (int i = 0; i < finalValue.Length; i++)
        {
            if (finalValue[i] != ' ')
                yield return new WaitForSeconds(timeBetweenLettersPrinted);

            _conversationText.text += finalValue[i];
        }

        _disableConversationCoroutine = StartCoroutine(DisableConversation());
    }

    private IEnumerator DisableConversation()
    {
        yield return new WaitForSeconds(5f);
        gameObject.SetActive(false);
    }
}
