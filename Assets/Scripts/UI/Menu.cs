using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private CanvasGroup _mainCanvasGroup, _buttonsCanvasGroup, _settingsCanvasGroup;

    [Space]
    [SerializeField] private Button _resumeGameButton;
    [SerializeField] private TextMeshProUGUI _title;

    [Space]
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private Slider _mainVolumeSlider, _musicVolumeSlider, _soundVolumeSlider;

    [Space]
    [SerializeField] private Vector2 _cameraSensitivityLimits;
    [SerializeField] private Slider _cameraSensitivitySlider;

    [Space]
    [SerializeField] private Player _player;
    [SerializeField] private CanvasGroup _playerUiCanvasGroup;

    [Space]
    [SerializeField] private Toggle[] _languageToggles;

    private void Start()
    {
        if (GameManager.Instance.DataPersistence.SettingsExist)
        {
            _mainVolumeSlider.value = GameManager.Instance.DataPersistence.GameSettingsData.MainVolume;
            _musicVolumeSlider.value = GameManager.Instance.DataPersistence.GameSettingsData.MusicVolume;
            _soundVolumeSlider.value = GameManager.Instance.DataPersistence.GameSettingsData.SoundVolume;

            _cameraSensitivitySlider.value = GameManager.Instance.DataPersistence.GameSettingsData.CameraSensitivity;

            _languageToggles[0].isOn = false;
            _languageToggles[(int)GameManager.Instance.DataPersistence.GameSettingsData.ELanguage].isOn = true;
            switch (GameManager.Instance.DataPersistence.GameSettingsData.ELanguage)
            {
                case ELanguage.ENGLISH:
                    Localization.Instance.SetLanguageToEnglish();
                    break;
                case ELanguage.SPANISH:
                    Localization.Instance.SetLanguageToSpanish();
                    break;
                case ELanguage.CATALAN:
                    Localization.Instance.SetLanguageToCatalan();
                    break;
            }
        }
    }

    private void OnEnable()
    {
        _player.PlayerInput.SwitchCurrentActionMap("UI");
        _playerUiCanvasGroup.alpha = 0;
        _resumeGameButton.gameObject.SetActive(GameManager.Instance.DataPersistence.GameExist || GameManager.Instance.isTutorialDone);
        OpenMenu();
    }

    private void OnDisable()
    {
        if (_player.PlayerInput.enabled)
            _player.PlayerInput.SwitchCurrentActionMap("Player");

        if (_playerUiCanvasGroup != null)
            _playerUiCanvasGroup.alpha = 1;
    }

    private void Update()
    {
        _title.fontSharedMaterial.SetFloat("_UnderlayOffsetX", Mathf.Clamp(Mathf.Cos(Time.timeSinceLevelLoad * 5f / Mathf.PI), -0.75f, 0.75f));
        _title.fontSharedMaterial.SetFloat("_UnderlayOffsetY", Mathf.Clamp(Mathf.Cos(Time.timeSinceLevelLoad * 7.5f / Mathf.PI), -0.75f, 0.75f));
        _title.transform.position += new Vector3(0, 5f * Mathf.Clamp(Mathf.Cos(Time.timeSinceLevelLoad / Mathf.PI), -0.75f, 0.75f) * Time.deltaTime);

        SetAllVolumes();
        SetMouseSensitivity();
    }

    public void NewGame()
    {
        if (!GameManager.Instance.DataPersistence.GameExist)
        {
            GameManager.Instance.NewGame();
            ExitMenu();
        }
        else
        {
            GameManager.Instance.DataPersistence.DeleteData();
            SceneManager.LoadScene(0);
        }
    }

    public void OpenMenu()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        StartCoroutine(CanvasFadeIn(_mainCanvasGroup));
    }

    public void ExitMenu()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        StartCoroutine(CanvasFadeOut(_mainCanvasGroup));
    }

    public void OpenSettings() => StartCoroutine(OnOpenSettings());

    private IEnumerator OnOpenSettings()
    {
        yield return StartCoroutine(CanvasFadeOut(_buttonsCanvasGroup));
        StartCoroutine(CanvasFadeIn(_settingsCanvasGroup));
    }

    public void CloseSettings() => StartCoroutine(OnCloseSettings());

    private IEnumerator OnCloseSettings()
    {
        yield return StartCoroutine(CanvasFadeOut(_settingsCanvasGroup));
        StartCoroutine(CanvasFadeIn(_buttonsCanvasGroup));
    }

    private IEnumerator CanvasFadeIn(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 0;
        canvasGroup.gameObject.SetActive(true);

        float alpha = 0;
        while (alpha < 1)
        {
            alpha += Time.deltaTime;
            canvasGroup.alpha = alpha;
            yield return null;
        }

        canvasGroup.interactable = true;
    }

    private IEnumerator CanvasFadeOut(CanvasGroup canvasGroup)
    {
        canvasGroup.interactable = false;

        float alpha = 1;
        while (alpha > 0)
        {
            alpha -= Time.deltaTime;
            canvasGroup.alpha = alpha;
            yield return null;
        }

        canvasGroup.gameObject.SetActive(false);
    }

    private void SetAllVolumes()
    {
        _audioMixer.SetFloat("MainVolume", Mathf.Log10(Mathf.Max(_mainVolumeSlider.value, 0.0001f)) * 20);
        _audioMixer.SetFloat("MusicVolume", Mathf.Log10(Mathf.Max(_musicVolumeSlider.value, 0.0001f)) * 20);
        _audioMixer.SetFloat("SoundVolume", Mathf.Log10(Mathf.Max(_soundVolumeSlider.value, 0.0001f)) * 20);
    }

    private void SetMouseSensitivity() => _player.CameraSensitivity = _cameraSensitivitySlider.value * (_cameraSensitivityLimits.y - _cameraSensitivityLimits.x) + _cameraSensitivityLimits.x;

    public void ExitGame() => Application.Quit();
}
