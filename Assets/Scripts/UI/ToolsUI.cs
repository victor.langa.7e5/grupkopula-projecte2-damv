using UnityEngine;
using UnityEngine.UI;

public class ToolsUI : MonoBehaviour
{
#nullable enable
    [SerializeField] private GameObject? _toolDisplay;

#nullable disable
    protected enum EToolUIState
    {
        NONE = -1,
        DISABLED = 0,
        ENABLED = 1,
    }

    [SerializeField] protected Image _image;
    protected EToolUIState _toolUIState;


    public void ResetState()
    {
        _toolUIState = EToolUIState.DISABLED;
    }

    public void SetActiveToolState()
    {
        _toolUIState = EToolUIState.ENABLED;
    }

    public virtual void UpdateLayout()
    {
        switch (_toolUIState)
        {
            case EToolUIState.NONE:
                break;
            case EToolUIState.DISABLED:
                _image.gameObject.transform.localScale = new Vector3(1,1,1);
                _image.color = new Color(0.35f, 0.75f, 0.457f, 1);
                if (_toolDisplay != null)
                    _toolDisplay.SetActive(false);

                break;
            case EToolUIState.ENABLED:
                if (_image.gameObject.transform.localScale == new Vector3(1, 1, 1))
                    _image.gameObject.transform.localScale *= 1.2f;

                _image.color = new Color(0.25f, 1, 0.557f, 1); 

                if (_toolDisplay != null)
                    _toolDisplay.SetActive(true);
                break;
            default:
                break;
        }
    }

    public virtual void UpdateDisplay()
    {   }
}
