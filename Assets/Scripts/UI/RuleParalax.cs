using UnityEngine;

public class RuleParalax : MonoBehaviour
{
    [SerializeField] RectTransform _rectTransform;
    float _startPosition;
    int _lenght; 
    int _currentOperand = 0;

    void Start()
    {
        _lenght = (int)_rectTransform.rect.height;
        _startPosition = _rectTransform.localPosition.y;
    }

    void Update()
    {
        float position = -GameManager.Instance.playerTransform.position.y + (_currentOperand * _lenght * 1.5f) + _startPosition;
        int operand = (int)(position / _lenght);
        
        if (position > _lenght * 1.5)
        {
            _currentOperand -= 2;
            position = ((_lenght * -2) * operand) - ((position % _lenght) - _lenght);
        }
        else if (position < -_lenght * 1.5)
        {
            _currentOperand += 2;
            position = ((_lenght * 2) * operand) + ((position % _lenght) - _lenght);
        }

        _rectTransform.localPosition = new Vector3(_rectTransform.localPosition.x, position, _rectTransform.localPosition.z);
    }
}
