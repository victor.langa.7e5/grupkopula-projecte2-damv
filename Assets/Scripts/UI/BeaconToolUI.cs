public class BeaconToolUI : ToolsUI
{
    public override void UpdateDisplay()
    {   }

    public override void UpdateLayout()
    {
        switch (_toolUIState)
        {
            case EToolUIState.NONE:
                break;
            case EToolUIState.DISABLED:
                _image.enabled = false;
                break;
            case EToolUIState.ENABLED:
                _image.enabled = true;
                break;
            default:
                break;
        }
    }
}
