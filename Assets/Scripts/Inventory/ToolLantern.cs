using System.Collections;
using UnityEngine;

public class ToolLantern : Tool
{
    [SerializeField] public Light Light;
    private bool _isSwitchedOn = false;

    public float Charge;
    [SerializeField] private float _dischargeValue;
    public float MaxCharge = 100;
    
    private Coroutine _wasteChargeCoroutine;
    [SerializeField] private float _circleCastRadius;
    [SerializeField] private float _agressionChargeFishDisorient;


    void OnEnable()
    {
        _isSwitchedOn = false;
        Light.enabled = false;
    }

    [ContextMenu("OnUse")]
    public override void OnUse()
    {        
        TryChangeState();
    }

    public bool TryChangeState()
    {
        gameObject.GetComponent<AudioSource>().Stop();
        gameObject.GetComponent<AudioSource>().Play();
        if (!_isSwitchedOn && Charge > 0)
        {
            _isSwitchedOn = true;
            Light.enabled = true;
            UIManager.Instance.UpdateToolDisplay(TOOL_TYPE);
            _wasteChargeCoroutine = StartCoroutine(WasteCharge());
            return true;
        }
        else if (_isSwitchedOn)
        {
            Light.enabled=false;
            _isSwitchedOn = false;
            UIManager.Instance.UpdateToolDisplay(TOOL_TYPE);
            StopCoroutine(_wasteChargeCoroutine);
            return true;
        }
        
        return false;
    }

    public void ChargeLantern(float charge)
    {
        Charge += charge;
        Charge = Mathf.Clamp(Charge, 0, MaxCharge);
        UIManager.Instance.UpdateToolDisplay(TOOL_TYPE);
    }

    private IEnumerator WasteCharge()
    {
        Charge -= _dischargeValue * Time.deltaTime;
        yield return null;
        if(Charge > 0)
            _wasteChargeCoroutine = StartCoroutine(WasteCharge());
        else
            Light.enabled = false;

        Physics.SphereCast(GameManager.Instance.playerTransform.position, _circleCastRadius, GameManager.Instance.playerTransform.forward, out RaycastHit rh);
        if (rh.collider != null)
            if (rh.collider.CompareTag("FishDisorient"))
                rh.collider.gameObject.GetComponent<FishDisorient>().AddAggressionCharge(_agressionChargeFishDisorient * Time.deltaTime);
    }

    public void LoseCharge(float loseChargePercentage)
    {
        Charge = Mathf.Clamp(Charge - (loseChargePercentage / 100 * MaxCharge), 0, MaxCharge);
        UIManager.Instance.UpdateToolDisplay(TOOL_TYPE);
    }

    public override void OnToolEquipped()
    {
        gameObject.SetActive(true);
        UIManager.Instance.UpdateToolDisplay(TOOL_TYPE);
    }

    public override void OnToolUnequipped()
    {
        gameObject.SetActive(false);
    }
}
