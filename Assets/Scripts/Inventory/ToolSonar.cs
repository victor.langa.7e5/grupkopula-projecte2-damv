using System;
using System.Collections;
using UnityEngine;

public class ToolSonar : Tool
{
    public float Cooldown;
    [HideInInspector] public float CurrentCooldown = 0;

    private DateTime _lastTimeSonarDisabled = DateTime.MinValue;

    [Space]
    [SerializeField] private float _soundRange;
    [SerializeField] private float _inverseResolution;

    [Space]
    [SerializeField] private GameObject pointObject;

    [Space]
    [SerializeField] private float _agressionChargeFishDisorient;

    private void OnEnable()
    {
        DateTime tempDate = _lastTimeSonarDisabled.AddSeconds(CurrentCooldown);
        if (tempDate > DateTime.Now)
        {
            CurrentCooldown = ((DateTimeOffset)tempDate).ToUnixTimeSeconds() - DateTimeOffset.Now.ToUnixTimeSeconds();
            StartCoroutine(CooldownCoroutine());
        }
        else 
            CurrentCooldown = 0;
    }

    private void OnDisable() => _lastTimeSonarDisabled = DateTime.Now;

    public override void OnUse()
    {
        TryUse();
    }

    [ContextMenu("UseSonar")]
    public bool TryUse()
    {
        if (CurrentCooldown <= 0)
        {
            gameObject.GetComponent<AudioSource>().Stop();
            gameObject.GetComponent<AudioSource>().Play();


            Ray ray = new();
            ray.origin = Vector3.zero;

            Vector3 direction = Vector3.right;

            int steps = Mathf.FloorToInt(360f / _inverseResolution);
            for (int x = 0; x < steps / 2; x++)
            {
                direction = Quaternion.Euler(Vector3.forward * _inverseResolution) * direction;
                for (int y = 0; y < steps; y++)
                {
                    direction = Quaternion.Euler(Vector3.right * _inverseResolution) * direction;
                    Physics.Raycast(transform.TransformPoint(transform.localPosition), ray.origin + direction, out RaycastHit rh, _soundRange);
                    Debug.DrawLine(transform.TransformPoint(transform.localPosition), (ray.origin + direction) * _soundRange);

                    if (rh.collider != null)
                    {
                        if (rh.collider.CompareTag("FishDisorient"))
                            rh.collider.gameObject.GetComponent<FishDisorient>().AddAggressionCharge(_agressionChargeFishDisorient);

                        Instantiate(pointObject, rh.point, pointObject.transform.rotation);
                    }

                    // To implement pool for the pointObjects
                }
            }

            CurrentCooldown = Cooldown;
            UIManager.Instance.UpdateToolDisplay(TOOL_TYPE);
            StartCoroutine(CooldownCoroutine());
        }

        return false;
    }

    IEnumerator CooldownCoroutine()
    {
        CurrentCooldown -= Time.deltaTime;
        yield return null;

        if (CurrentCooldown > 0)
            StartCoroutine(CooldownCoroutine());
    }

    public override void OnToolEquipped()
    {
        gameObject.SetActive(true);
        UIManager.Instance.UpdateToolDisplay(TOOL_TYPE);
    }

    public override void OnToolUnequipped()
    {
        gameObject.SetActive(false);
    }
}
