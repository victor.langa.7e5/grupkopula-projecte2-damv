using System.Collections;
using UnityEngine;

public class SonarPoint : MonoBehaviour
{
    [SerializeField] float _secondsInCycle;
    [SerializeField] float _secondsStayingInWorld;

    [SerializeField] float _distanceSpawnTimeRate;

    void OnEnable() => StartCoroutine(AppearAndDisappear());

    IEnumerator AppearAndDisappear()
    {
        Vector3 increaseRate = new Vector3(transform.localScale.x * _secondsInCycle / 100, transform.localScale.y * _secondsInCycle / 100, transform.localScale.z * _secondsInCycle / 100);
        transform.localScale = Vector3.zero;

        yield return new WaitForSeconds(_distanceSpawnTimeRate * Vector3.Distance(transform.position, GameManager.Instance.playerTransform.position));

        for (int i = 0; i < 100; i++)
        {
            transform.localScale = new Vector3(transform.localScale.x + increaseRate.x, transform.localScale.y + increaseRate.y, transform.localScale.z + increaseRate.z);
            yield return new WaitForSeconds(_secondsInCycle / 100);
        }

        yield return new WaitForSeconds(_secondsStayingInWorld);

        for (int i = 0; i < 100; i++)
        {
            transform.localScale = new Vector3(transform.localScale.x - increaseRate.x, transform.localScale.y - increaseRate.y, transform.localScale.z - increaseRate.z);
            yield return new WaitForSeconds(_secondsInCycle / 100);
        }

        Destroy(gameObject);
    }
}
