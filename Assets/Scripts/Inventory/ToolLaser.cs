using System.Collections;
using UnityEngine;

public class ToolLaser : Tool
{
    public Light Light;

    void Start()
    {
        TOOL_TYPE = EPlayerTool.LASER;
        Light.enabled = false;
    }

    private void OnEnable() => Light.enabled = false;

    IEnumerator ThrowRaycast()
    {
        Debug.DrawLine(transform.position, transform.position + transform.TransformDirection(Vector3.left) * 100, Color.red);
        
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), out RaycastHit rh, Mathf.Infinity))
            if (rh.collider.gameObject.TryGetComponent(out ILasereable component))
                component.OnLaserInteract();

        yield return null;

        if (Light.enabled)
            StartCoroutine(ThrowRaycast());
    }


    public override void OnToolEquipped()
    {
        gameObject.SetActive(true);
        UIManager.Instance.UpdateToolDisplay(TOOL_TYPE);
    }

    public override void OnToolUnequipped()
    {
        gameObject.SetActive(false);
    }

    [ContextMenu("OnUse")]
    public override void OnUse()
    {
        gameObject.GetComponent<AudioSource>().Stop();
        gameObject.GetComponent<AudioSource>().Play();
        Light.enabled = !Light.enabled;
        StartCoroutine(ThrowRaycast());
        UIManager.Instance.UpdateToolDisplay(TOOL_TYPE);
    }
}
