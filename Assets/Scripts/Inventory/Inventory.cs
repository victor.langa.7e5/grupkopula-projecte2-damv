using UnityEngine;
using UnityEngine.InputSystem;

public class Inventory : MonoBehaviour
{
    [SerializeField] private Tool[] _tools; // The tools in the editor must be introduced in the same order as the EPlayerTool enum
    public Tool.EPlayerTool _currentTool { get; private set; }
    [SerializeField] private Animator _animator;

    private void OnEnable() => GameManager.Instance.OnChangePlayerStateEvent += OnChangePlayerState;

    private void OnDisable() => GameManager.Instance.OnChangePlayerStateEvent -= OnChangePlayerState;

    private void OnChangePlayerState(Player.EPlayerState playerState)
    {
        switch (playerState)
        {
            case Player.EPlayerState.DISORIENTED:
                UnequipTool();
                break;
            case Player.EPlayerState.NORMAL:
                EquipTool();
                break;
        }
    }

    public void OnSwitchToLaser() => OnSwitchTool(Tool.EPlayerTool.LASER);
    public void OnSwitchToCompass() => OnSwitchTool(Tool.EPlayerTool.COMPASS);
    public void OnSwitchToLantern() => OnSwitchTool(Tool.EPlayerTool.LANTERN);
    public void OnSwitchToSonar() => OnSwitchTool(Tool.EPlayerTool.SONAR);

    private void OnSwitchTool(Tool.EPlayerTool toolToEquip)
    {
        _animator.SetTrigger("ChangeWeapon");
        UnequipTool();
        _currentTool = toolToEquip;
        UIManager.Instance.UpdatePlayerLayout(_currentTool);
        EquipTool();
    }

    public void OnSwitchTool(InputAction.CallbackContext context)
    {
        if (context.performed && gameObject.activeSelf)
        {
            _animator.SetTrigger("ChangeWeapon");

            UnequipTool();

            _currentTool = (Tool.EPlayerTool)(((int)_currentTool + (context.ReadValue<Vector2>().y > 0 ? 1 : -1)) % 4);
            if ((int)_currentTool == -1)
                _currentTool = (Tool.EPlayerTool)3;

            UIManager.Instance.UpdatePlayerLayout(_currentTool);
            EquipTool();
        }
    }

    public void UseTool(InputAction.CallbackContext context)
    {
        if (context.performed && gameObject.activeSelf)
            _tools[(int)_currentTool].OnUse();
    }

    private void EquipTool() => _tools[(int)_currentTool].OnToolEquipped();

    private void UnequipTool() => _tools[(int)_currentTool].OnToolUnequipped();
}
