using UnityEngine;

public abstract class Tool : MonoBehaviour
{
    public enum EPlayerTool
    { 
        NONE = -1,
        SONAR = 0,
        LANTERN = 1,
        LASER = 2,
        COMPASS = 3
    }

    public EPlayerTool TOOL_TYPE;

    public abstract void OnUse();
    public abstract void OnToolEquipped();
    public abstract void OnToolUnequipped();
}
