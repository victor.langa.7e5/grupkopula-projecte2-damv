using UnityEngine;

public class ToolCompass : Tool
{
#nullable enable
    private Transform? _rechargeRock;
#nullable disable
    [SerializeField] Animator _anim;
    [SerializeField] private Transform _nidle;

    private void OnEnable() => GameManager.Instance.OnPointToAimUpdated += SetPointToAim;

    private void OnDisable() => GameManager.Instance.OnPointToAimUpdated -= SetPointToAim;

    private void Update()
    {
        if(_rechargeRock)
            _nidle.LookAt(_rechargeRock);
    }

    public override void OnUse() { }

    public override void OnToolEquipped()
    {
        gameObject.SetActive(true);
        SetPointToAim();
    }

    public override void OnToolUnequipped()
    {
        gameObject.SetActive(false);
    }

    private void SetPointToAim()
    {
        _rechargeRock = GameManager.Instance.PointToAim;
        if (!_rechargeRock)
            _anim.enabled = true;
        else
            _anim.enabled = false;
    }
}