using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class Beacon : MonoBehaviour
{
    private ToolBeaconManager _toolBeaconManager;

    [SerializeField] private Light _light;

    [SerializeField] private TextMeshProUGUI _collectText;

    private bool _canBeCollected = false;

    private void Start() 
    {
	    _toolBeaconManager = GameObject.Find("BeaconManager").GetComponent<ToolBeaconManager>();
        //_inputSystem = GameObject.Find("Player").GetComponent<InputSystem>().
    }

    [ContextMenu("CollectBeacon")]
    public void CollectBeacon(InputAction.CallbackContext context)
    {
        if (context.performed && _canBeCollected)
        {
            _toolBeaconManager.AddBeacon();
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider c)
    { 
        if (c.gameObject.CompareTag("Player"))
        {
            _toolBeaconManager._nearBeacon = this.gameObject;
            _canBeCollected = true;
            _collectText.transform.position = Camera.main.WorldToScreenPoint(transform.position, Camera.MonoOrStereoscopicEye.Mono);
            _collectText.gameObject.SetActive(true);
        }
    }

    private void OnTriggerStay(Collider c)
    {
        if (c.gameObject.CompareTag("Player"))
            _collectText.transform.position = Camera.main.WorldToScreenPoint(transform.position, Camera.MonoOrStereoscopicEye.Mono);
    }

    private void OnTriggerExit(Collider c)
    {
        if (c.gameObject.CompareTag("Player"))
        {
            _toolBeaconManager._nearBeacon = null;
            _canBeCollected = false;
            _collectText.gameObject.SetActive(false);
        }
    }
}
