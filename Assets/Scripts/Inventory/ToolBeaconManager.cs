using UnityEngine;
using UnityEngine.InputSystem;

public class ToolBeaconManager : Tool
{
#nullable enable
    public GameObject? _nearBeacon { private get; set; }

#nullable disable
    [SerializeField] private GameObject _beaconObject;
    [SerializeField] private int _totalBeacons;
    public void UseBeacon(InputAction.CallbackContext context)
    {
        if (context.performed && GameManager.Instance.inventory.gameObject.activeSelf)
            OnUse();
    }

    public override void OnUse()
    {
        if (_nearBeacon == null)
        {
            TryUseBeacon();
        }
        else
        {
            CollectBeacon();
        }
    }

    public bool TryUseBeacon()
    {
        if (_totalBeacons > 0)
        {
            Instantiate(_beaconObject, transform.position, _beaconObject.transform.rotation);
            _totalBeacons--;
            UIManager.Instance.UpdatePlayerLayout(EPlayerTool.NONE, _totalBeacons);
            return true;
        }

        return false;
    }

    public override void OnToolEquipped()
    {
        gameObject.SetActive(true);
    }

    public override void OnToolUnequipped()
    {
        gameObject.SetActive(false);
    }


    public void CollectBeacon()
    {
        Destroy(_nearBeacon.gameObject);
        AddBeacon();
    }

    public void AddBeacon()
    {
        _totalBeacons++;
        UIManager.Instance.UpdatePlayerLayout(EPlayerTool.NONE, _totalBeacons);
    }
}
