using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerColliderTerrainControl : MonoBehaviour
{

    [SerializeField] private TerrainCollider _colliderTerrain;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _colliderTerrain.enabled = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _colliderTerrain.enabled = true;
        }
    }
}
