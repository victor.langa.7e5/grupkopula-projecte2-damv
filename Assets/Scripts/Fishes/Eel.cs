using UnityEngine;

public class Eel : MonoBehaviour
{
    [SerializeField] private float _forceMagnitude;
    [SerializeField] ToolLantern _toolLantern;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //Cambiar el estado del jugador
            GameManager.Instance.ChangePlayerStateEvent(Player.EPlayerState.ELECTRIFIED);
            _toolLantern.LoseCharge(33);

            //Empujarlo ligeramente
            var forceDirection = transform.position - collision.transform.position;
            forceDirection.Normalize();

            collision.rigidbody.AddForce(-forceDirection * _forceMagnitude, ForceMode.Impulse);
        }
    }
}
