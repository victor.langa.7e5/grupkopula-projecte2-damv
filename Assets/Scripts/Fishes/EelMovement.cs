using System.Collections;
using UnityEngine;

public class EelMovement : MonoBehaviour
{
    [SerializeField] private Rail _rail;
    [SerializeField] private Transform[] _bones;
    
    [Space]
    private int _currentSegment = 0;
    [SerializeField] private float _speedRatio;
    [SerializeField] private float _rotationRatio;
    [SerializeField] private float _timeBetweenBoneRotations;

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, _rail.nodes[_currentSegment + 1].position, Time.deltaTime * _speedRatio);
        if (Equals(transform.position, _rail.nodes[_currentSegment + 1].position))
        {
            StartCoroutine(RotateEel());
            _currentSegment = (_currentSegment + 1) % (_rail.nodes.Length - 1);
        }
    }

    private IEnumerator RotateEel()
    {
        while (Vector3.Distance(_bones[0].eulerAngles, _rail.nodes[_currentSegment].eulerAngles) > 0.001f)
        {
            Vector3 lastEulerAngles = _bones[0].eulerAngles;
            _bones[0].eulerAngles = new Vector3 
            (
                Mathf.LerpAngle(_bones[0].eulerAngles.x, _rail.nodes[_currentSegment].eulerAngles.x, _rotationRatio * Time.deltaTime),
                Mathf.LerpAngle(_bones[0].eulerAngles.y, _rail.nodes[_currentSegment].eulerAngles.y, _rotationRatio * Time.deltaTime),
                Mathf.LerpAngle(_bones[0].eulerAngles.z, _rail.nodes[_currentSegment].eulerAngles.z, _rotationRatio * Time.deltaTime)
            );

            _bones[1].eulerAngles -= _bones[0].eulerAngles - lastEulerAngles;
            StartCoroutine(RedoChildRotation(1, _bones[0].eulerAngles - lastEulerAngles));

            yield return null;
        }
    }

    private IEnumerator RedoChildRotation(int currentBone, Vector3 rotationToRedo)
    {
        yield return new WaitForSeconds(_timeBetweenBoneRotations);
        _bones[currentBone].eulerAngles += rotationToRedo;

        if (currentBone < _bones.Length - 1)
        {
            _bones[currentBone + 1].eulerAngles -= rotationToRedo;
            StartCoroutine(RedoChildRotation(currentBone + 1, rotationToRedo));
        }
    }
}


