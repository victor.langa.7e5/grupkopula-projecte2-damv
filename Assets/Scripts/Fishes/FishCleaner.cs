using System.Collections;
using UnityEngine;

public class FishCleaner : MonoBehaviour
{
    [SerializeField] private Rigidbody _rb;
    [SerializeField] private float _rotationRatioPerFrame;
    [SerializeField] private float _speedRatioPerFrame;

    [Space]
    [SerializeField] private float _timeEatingSeaweed;
    private GameObject _target = null;
    private GameObject _seaweedObject = null;
    private bool _isEating = false;

    public IEnumerator FollowTarget()
    {
        while (_target.activeInHierarchy)
        {
            Quaternion targetRotation = Quaternion.LookRotation(_target.transform.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, _rotationRatioPerFrame * Time.deltaTime);
            _rb.AddForce(_speedRatioPerFrame * Time.deltaTime * (_target.transform.position - transform.position).normalized);
            yield return null;
        }

        _target = null;
        _isEating = false;
    }

    private IEnumerator EatSeaweed(GameObject seaweed)
    {
        float timeSinceStart = 0;

        while (timeSinceStart < _timeEatingSeaweed && _target != null)
        {
            seaweed.transform.localScale = new Vector3(seaweed.transform.localScale.x, seaweed.transform.localScale.y - (seaweed.transform.localScale.y * Time.deltaTime / _timeEatingSeaweed), seaweed.transform.localScale.z);
            timeSinceStart += Time.deltaTime;
            yield return null;
        }

        StopAllCoroutines();
        seaweed.SetActive(false);
        //Destroy(seaweed);
        _target = null;
        _isEating = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Seaweed") && _target == null)
        {
            _target = other.gameObject;
            _seaweedObject = _target.transform.parent.gameObject;
            StartCoroutine(FollowTarget());
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Seaweed") && !_isEating && collision.gameObject == _seaweedObject)
        {
            _isEating = true;
            StartCoroutine(EatSeaweed(collision.gameObject));
        }
    }
}
