using System.Collections;
using UnityEngine;

public class FishDisorient : MonoBehaviour
{
    private enum EFishDisorientStates
    { 
        NONE,
        NAVIGATING,
        PERSECUTION,
        DISORIENTING_PLAYER
    }

    [SerializeField] private Rigidbody _rb;

    [Space]
    [SerializeField] private Vector3[] _positionsToTravel;
    private int _currentIndex = 0;

    [Space]
    private EFishDisorientStates _currentState = EFishDisorientStates.NAVIGATING;
    [SerializeField] private float _rotationRatioPerFrame;
    [SerializeField] private float _speedRatioPerFrame;
    [SerializeField] private float _speedMultiplierWhenPersecuting;

    [Space]
    [SerializeField] private float _currentAgressionCharge;
    [SerializeField] private float _agressionLimit;

    [Space]
    [Range(0, 1)]
    [SerializeField] private double _consequenceRatio;


    [ContextMenu("GoAgressive")]
    private void GoAgressive() => _currentState = EFishDisorientStates.PERSECUTION;

    private void Update()
    {
        if (_currentAgressionCharge > _agressionLimit)
            _currentState = EFishDisorientStates.PERSECUTION;

        Quaternion targetRotation = new();

        switch (_currentState)
        {
            case EFishDisorientStates.NAVIGATING:
                targetRotation = Quaternion.LookRotation(_positionsToTravel[_currentIndex] - transform.position);
                Navigate();
                break;
            case EFishDisorientStates.PERSECUTION:
                targetRotation = Quaternion.LookRotation(GameManager.Instance.playerTransform.position - transform.position);
                Persecute();
                break;
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, _rotationRatioPerFrame * Time.deltaTime);
        LowerAggressionCharge();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && _currentState == EFishDisorientStates.PERSECUTION)
        {
            _currentState = EFishDisorientStates.DISORIENTING_PLAYER;
            if (Random.Range(0f, 1f) > _consequenceRatio)
                StartCoroutine(Stun());
            else
                StartCoroutine(Transport(collision.gameObject.transform));
            _currentAgressionCharge = 0;
        }
    }

    private void Navigate()
    {
        _rb.AddForce(_speedRatioPerFrame * Time.deltaTime * (_positionsToTravel[_currentIndex] - transform.position).normalized);
        if (Vector3.Distance(transform.position, _positionsToTravel[_currentIndex]) < 1)
            _currentIndex = (_currentIndex + 1) % _positionsToTravel.Length;
    }

    private void Persecute()
    {
        _rb.AddForce(_speedMultiplierWhenPersecuting * _speedRatioPerFrame * Time.deltaTime * (GameManager.Instance.playerTransform.position - transform.position).normalized);
    }

    public void AddAggressionCharge(float value) => _currentAgressionCharge += value;

    private void LowerAggressionCharge() => _currentAgressionCharge = Mathf.Max(_currentAgressionCharge - Time.deltaTime, 0);

    private IEnumerator Stun()
    {
        GameManager.Instance.ChangePlayerStateEvent(Player.EPlayerState.DISORIENTED);
        yield return new WaitForSeconds(2f);
        GameManager.Instance.ChangePlayerStateEvent(Player.EPlayerState.NORMAL);
    }

    IEnumerator Transport(Transform playerTransform)
    {
        GameManager.Instance.ChangePlayerStateEvent(Player.EPlayerState.DISORIENTED);

        Vector3 initialLocalScale = playerTransform.localScale;

        playerTransform.SetParent(transform);

        int newPosition = (_currentIndex + (_positionsToTravel.Length / 2)) % _positionsToTravel.Length;
        transform.position = _positionsToTravel[newPosition];
        _rb.velocity = Vector3.zero;
        _rb.angularVelocity = Vector3.zero;

        yield return new WaitForSeconds(5);

        _currentIndex = newPosition;
        _currentState = EFishDisorientStates.NAVIGATING;
        playerTransform.SetParent(null);
        playerTransform.localScale = initialLocalScale;

        GameManager.Instance.ChangePlayerStateEvent(Player.EPlayerState.NORMAL);
    }
}
