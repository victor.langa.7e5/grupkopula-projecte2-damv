using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DataPersistence : MonoBehaviour
{
    
    private string _directoryPath;

    [SerializeField] Player _player;
    [SerializeField] List<GameObject> _seaweeds;

    [Space]
    [SerializeField] Slider _mainVolume, _musicVolume, _soundVolume, _cameraSensitivity;

    public bool GameExist { get; private set; } = false;
    public bool SettingsExist { get; private set; } = false;
    public GameplayData GameplayData { get; private set; }
    public GameSettingsData GameSettingsData { get; private set; }

    //Application.persistentDataPath; <== Para Build
    //Application.DataPath; <== Para Editor
    private void Awake()
    {
        _directoryPath = Application.persistentDataPath;
    }
    public void SaveAllData()
    {
        //--------------------------GameSettings--------------------------//
        SaveData(JsonUtility.ToJson(GetGameSettingsData()), "SettingsData.json");

        //--------------------------GameplayData--------------------------//
        if (GameManager.Instance.isTutorialDone)
            SaveData(JsonUtility.ToJson(GetGameplayData()), "GameplayData.json");
    }

    private GameSettingsData GetGameSettingsData()
    {
        GameSettingsData gameSettingsData = new GameSettingsData();
        gameSettingsData.MainVolume = _mainVolume.value;
        gameSettingsData.MusicVolume = _musicVolume.value;
        gameSettingsData.SoundVolume = _soundVolume.value;
        gameSettingsData.CameraSensitivity = _cameraSensitivity.value;
        gameSettingsData.ELanguage = Localization.Instance.CurrentLanguage;

        return gameSettingsData;
    }

    private GameplayData GetGameplayData()
    {
        GameplayData gameplayData = new GameplayData();
        gameplayData.seaweeds = new List<GameplayData.Seaweeds>();
        gameplayData.playerTransformPosition = _player.transform.position;

        foreach (var seaweed in _seaweeds)
            gameplayData.seaweeds.Add(new GameplayData.Seaweeds(seaweed, seaweed.activeSelf));

        return gameplayData;
    }


    private void SaveData(string data, string fileName)
    {
        string filePath = _directoryPath + "/" + fileName;

        if (File.Exists(filePath))
            File.Delete(filePath);
        
        FileStream fileStream = new FileStream(filePath, FileMode.Create);
        using (StreamWriter sw = new StreamWriter(fileStream))
            sw.Write(data); 
    }

   
    public void LoadData()
    {
        _directoryPath = Application.persistentDataPath; //<--Hardcoded 
        if (File.Exists(_directoryPath + "/SettingsData.json"))
        {
            //--------------------------GameSettings--------------------------//
            using (StreamReader reader = new StreamReader(_directoryPath + "/SettingsData.json"))
                GameSettingsData = JsonUtility.FromJson<GameSettingsData>(reader.ReadToEnd());
        }

        if (File.Exists(_directoryPath + "/GameplayData.json"))
        {
            //--------------------------GameplayData--------------------------//
            using (StreamReader reader = new StreamReader(_directoryPath + "/GameplayData.json"))
                GameplayData = JsonUtility.FromJson<GameplayData>(reader.ReadToEnd());
        }

        GameExist = File.Exists(_directoryPath + "/GameplayData.json");
        SettingsExist = File.Exists(_directoryPath + "/SettingsData.json");

    }

    public void DeleteData() => File.Delete(_directoryPath + "/GameplayData.json");
}

//GAME SETTINGS
//Camara Sensitivity
//Main Volume
//Music Volume
//Sound Volume
//Lenguage
[Serializable]
public class GameSettingsData
{
    public float MainVolume;
    public float MusicVolume;
    public float SoundVolume;
    public float CameraSensitivity;
    public ELanguage ELanguage;
}


//GAMEPLAY DATA
//Player position
//Seaweeds
//TutorialDone
[Serializable]
public class GameplayData
{
    public Vector3 playerTransformPosition;
    public List<Seaweeds> seaweeds;

    [Serializable]
    public class Seaweeds
    {
        public GameObject SeaweedGameObject;
        public bool IsEnabled;
        public Seaweeds(GameObject seaweedGameObject, bool isEnabled)
        {
            SeaweedGameObject = seaweedGameObject;
            IsEnabled = isEnabled;
        }
    }
}


