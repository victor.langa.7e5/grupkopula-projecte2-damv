using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LocalizatedValue : MonoBehaviour
{
    [SerializeField] private string _localizationKey;
    private TextMeshProUGUI _text;

    void Awake() => _text = GetComponent<TextMeshProUGUI>();

    private void OnEnable()
    {
        GetLocalizatedValue();
        Localization.Instance.OnLanguageChange += GetLocalizatedValue;
    }

    private void OnDisable() => Localization.Instance.OnLanguageChange -= GetLocalizatedValue;

    public void GetLocalizatedValue()
    {
        Dictionary<string, string> localizatedValues = Localization.Instance.GetCurrentLanguageData();

        localizatedValues.TryGetValue(_localizationKey, out string finalValue);
        _text.text = finalValue;
    }
}
