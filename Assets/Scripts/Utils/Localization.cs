using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Localization : MonoBehaviour
{
    
    private static Localization _instance;
    public static Localization Instance { get { return _instance; } }

    [SerializeField] private TextAsset _english;
    [SerializeField] private TextAsset _spanish;
    [SerializeField] private TextAsset _catalan;

    private Dictionary<string, string> _englishData;
    private Dictionary<string, string> _spanishData;
    private Dictionary<string, string> _catalanData;

    public ELanguage CurrentLanguage { get; private set; } = ELanguage.ENGLISH;
    public event Action OnLanguageChange;

    private void Awake()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;

        _englishData = _english.text.Split(Environment.NewLine).ToDictionary(s => s.Split('=')[0], s => s.Split('=')[1]);
        _spanishData = _spanish.text.Split(Environment.NewLine).ToDictionary(s => s.Split('=')[0], s => s.Split('=')[1]);
        _catalanData = _catalan.text.Split(Environment.NewLine).ToDictionary(s => s.Split('=')[0], s => s.Split('=')[1]);
    }

    public void SetLanguageToEnglish() => ChangeLanguage(ELanguage.ENGLISH);

    public void SetLanguageToSpanish() => ChangeLanguage(ELanguage.SPANISH);

    public void SetLanguageToCatalan() => ChangeLanguage(ELanguage.CATALAN);

    private void ChangeLanguage(ELanguage language)
    {
        CurrentLanguage = language;
        OnLanguageChange?.Invoke();
    }

    public Dictionary<string, string> GetCurrentLanguageData()
    {
        switch (CurrentLanguage)
        {
            case ELanguage.ENGLISH:
                return _englishData;
            case ELanguage.SPANISH:
                return _spanishData;
            case ELanguage.CATALAN:
                return _catalanData;
        }

        return null;
    }
}

[Serializable]
public enum ELanguage
{ 
    NONE = -1,
    ENGLISH,
    SPANISH,
    CATALAN
}