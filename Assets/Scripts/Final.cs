using UnityEngine;

public class Final : MonoBehaviour
{
    [SerializeField] private GameObject _finalManager;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            _finalManager.SetActive(true);
    }
}
