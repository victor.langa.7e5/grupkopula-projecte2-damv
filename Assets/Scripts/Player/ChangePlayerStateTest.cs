using System.Collections;
using UnityEngine;

public class ChangePlayerStateTest : MonoBehaviour
{
    [ContextMenu("ChangePlayerState")]
    public void tal()
    {
        StartCoroutine(ChangePlayerState());
    }

    public IEnumerator ChangePlayerState()
    {
        GameManager.Instance.ChangePlayerStateEvent(Player.EPlayerState.DISORIENTED);
        yield return new WaitForSeconds(5f);
        GameManager.Instance.ChangePlayerStateEvent(Player.EPlayerState.NORMAL);
    }

}
