using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    public enum EPlayerState
    {
        NONE = -1,
        NORMAL = 0,
        DISORIENTED = 1,
        ELECTRIFIED = 2
    }

    private EPlayerState _playerState = EPlayerState.NORMAL;

    [SerializeField] float _acceleration;
    [SerializeField] float _maxSpeed;

    [SerializeField] private Rigidbody _rb;
    private Vector3 _playerAcceleration3D;
    private int _playerAscendDescendInput;

    public float CameraSensitivity;
    [SerializeField] float _minMaxAngle = 80f;

    private Vector2 _currentRotation;

    public PlayerInput PlayerInput;

    private void OnEnable() => GameManager.Instance.OnChangePlayerStateEvent += ChangePlayerState;
    private void OnDisable() => GameManager.Instance.OnChangePlayerStateEvent -= ChangePlayerState;


    void Start() => _currentRotation = new Vector2(transform.localRotation.eulerAngles.y, transform.localRotation.eulerAngles.x);

    private void FixedUpdate()
    {
        if (_playerState == EPlayerState.NORMAL)
        {
            //Player normal movement
            _rb.AddForce(transform.TransformVector(Vector3.forward) * _playerAcceleration3D.z);
            _rb.AddForce(transform.TransformVector(Vector3.right) * _playerAcceleration3D.x);

            //Player Ascend / Descend movement
            _rb.AddForce(_acceleration * _playerAscendDescendInput * Vector3.up);
    
            //Need to clamp cause there can be 2 inputs
            _rb.velocity = Vector3.ClampMagnitude(_rb.velocity, _maxSpeed);

            transform.rotation = Quaternion.Euler(-_currentRotation.y, _currentRotation.x, 0);
        }
    }

    //Update the player acceleration values.
    public void OnMove(InputAction.CallbackContext context)
    {
        _playerAcceleration3D.x = context.ReadValue<Vector2>().x * _acceleration;
        _playerAcceleration3D.z = context.ReadValue<Vector2>().y * _acceleration;
    }
    
    public void OnAscendDescend(InputAction.CallbackContext context)
    {
        _playerAscendDescendInput = context.ReadValue<float>().ConvertTo<int>();
    }

    //Equals the player rotation to the camera rotation.
    public void OnLook(InputAction.CallbackContext context)
    {
        _currentRotation += CameraSensitivity * Time.deltaTime * new Vector2(context.ReadValue<Vector2>().x, context.ReadValue<Vector2>().y);
        _currentRotation.y = Mathf.Clamp(_currentRotation.y, -_minMaxAngle, _minMaxAngle);
    }

    public void OnOpenMenu(InputAction.CallbackContext context)
    {
        if (context.performed && GameManager.Instance.isTutorialDone)
            GameManager.Instance.Menu.SetActive(true);
    }

    private void ChangePlayerState(EPlayerState newplayerState)
    {
        _playerState = newplayerState;
        OnChangePlayerState();
    }

    private void OnChangePlayerState()
    {
        switch (_playerState)
        {
            case EPlayerState.NONE:
                break;
            case EPlayerState.NORMAL:
                PlayerInput.enabled = true;
                break;
            case EPlayerState.DISORIENTED:
                PlayerInput.enabled = false;
                break;
            case EPlayerState.ELECTRIFIED:
                StartCoroutine(OnElectrified());
                break;
            default:
                break;
        }
    }

    private IEnumerator OnElectrified()
    {
        PlayerInput.enabled = false;
        yield return new WaitForSeconds(1f);
        GameManager.Instance.ChangePlayerStateEvent(EPlayerState.NORMAL);
        PlayerInput.enabled = true;
    }
}
