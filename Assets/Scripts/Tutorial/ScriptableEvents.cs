using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class ScriptableEvents : MonoBehaviour
{
    [SerializeField] private Conversation _conversation;
    [SerializeField] private Instructions _instructions;

    [Space]
    [SerializeField] private Block[] _blocks;

    [Serializable]
    public struct Block
    {
#if UNITY_EDITOR
        public string Name;
#endif
        public string ConversationKey;
        public Conversation.ESpeaker Speaker;
        public float TimeBetweenLettersPrinted;
        public float TimeUntilNextBlock;
        public InputParameters InputParameters;
        public UnityEvent Event;
    }

    [Serializable]
    public struct InputParameters
    {
        public string PopUpKey;
        public ETutorialKeyCodes[] RequiredInputs;
    }

    public enum ETutorialKeyCodes
    {
        None = -1,
        W = 119,
        A = 97,
        S = 115,
        D = 100,
        LeftControl = 306,
        Space = 32,
        E = 101,
        Keypad1 = 49,
        Keypad2 = 50,
        Keypad3 = 51,
        Keypad4 = 52,
        LeftClick = 323
    }

    public void Start() => StartCoroutine(DoBlocks());

    public IEnumerator DoBlocks()
    {
        foreach (var block in _blocks)
        {
            if (block.ConversationKey != "")
            {
                _conversation.gameObject.SetActive(true);
                StartCoroutine(_conversation.ShowSubtitle(block.ConversationKey, block.TimeBetweenLettersPrinted, block.Speaker));
            }

            if (block.InputParameters.PopUpKey != "" || block.InputParameters.RequiredInputs.Length > 0)
            {
                _instructions.gameObject.SetActive(true);
                StartCoroutine(_instructions.WaitForRequiredInputs(block.InputParameters.PopUpKey, block.InputParameters.RequiredInputs));

                while (!_instructions.AllInputReceived)
                    yield return null;

                _instructions.AllInputReceived = false;
            }

            block.Event?.Invoke();

            yield return new WaitForSeconds(block.TimeUntilNextBlock);
        }
    }
}



