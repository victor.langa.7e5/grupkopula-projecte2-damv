using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem;

public class UnityEvents : MonoBehaviour
{
    [SerializeField] private Animator _playerAnimator;
    [SerializeField] private RuntimeAnimatorController _playerController;
    [SerializeField] private PlayerInput _playerInput;
    [SerializeField] private Player _player;
    [SerializeField] private Inventory _inventory;
    [SerializeField] private GameObject _finalLightSphere;
    [SerializeField] private GameObject _credits;
    [SerializeField] private BoxCollider _playerCollider;
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _audioClip;
    [SerializeField] private MusicManager _musicManager;
    [SerializeField] private AudioClip _audioClipTutorial, _audioClipEnd;

    public void ActivateUiCanvas() => GameManager.Instance.uiCanvas.SetActive(true);

    public void ActivatePlayerInput() => _playerInput.enabled = true;

    public void ActivatePlayer() => _player.enabled = true;

    public void ActivateInventory() => _inventory.gameObject.SetActive(true);

    public void ActivateLimitColliders() => GameManager.Instance.limitColliders.SetActive(true);

    public void SetAnimatorController() => _playerAnimator.runtimeAnimatorController = _playerController;

    public void EnablePlayerCollider() => _playerCollider.enabled = true;

    public void TutorialHasEnded() => GameManager.Instance.isTutorialDone = true;

    public void ActivateAudioSource() => GameManager.Instance.PlayerAudioSource.enabled = true;

    public void ChangeMusicManagerState(bool isEnabled) => GameManager.Instance.MusicManager.enabled = isEnabled;

    public void PlayTutorialMusic()
    {
        GameManager.Instance.MusicAudioSource.clip = _audioClipTutorial;
        GameManager.Instance.MusicAudioSource.Play();
    }

    public void PlayEndMusic()
    {
        GameManager.Instance.MusicAudioSource.Stop();
        GameManager.Instance.MusicAudioSource.clip = _audioClipEnd;
        GameManager.Instance.MusicAudioSource.Play();
    }

    public void EnterFinal()
    {
        GameManager.Instance.isTutorialDone = false;
        _player.enabled = false;
        _playerInput.enabled = false;
        _inventory.enabled = false;
        GameManager.Instance.uiCanvas.SetActive(false);

        _musicManager.gameObject.SetActive(false);
        _audioSource.clip = _audioClip;
        _audioSource.Play();

        GetCloseToFinalSphere();
    }

    private async void GetCloseToFinalSphere()
    {
        float time = 0;
        while (time < 10)
        {
            _player.transform.position = Vector3.MoveTowards(_player.transform.position, _finalLightSphere.transform.position, Time.deltaTime);
            var targetRotation = Quaternion.LookRotation(_finalLightSphere.transform.position - _player.transform.position);
            _player.transform.rotation = Quaternion.Slerp(_player.transform.rotation, targetRotation, Time.deltaTime);
            time += Time.deltaTime;
            await Task.Yield();
        }

        FinalTp();
    }

    private void FinalTp()
    {
        _player.transform.position = new Vector3(0, 500, 0);
        _finalLightSphere.transform.position = new Vector3(0, 500);
        Fall();
    }

    private async void Fall()
    {
        while (_player.transform.position.y > 0)
        {
            _player.transform.LookAt(_finalLightSphere.transform.position, _player.transform.up);
            _player.transform.position -= new Vector3(0, Time.deltaTime * 15, 0);
            await Task.Yield();
        }
    }

    public async void Credits()
    {
        _credits.SetActive(true);

        var panel = _credits.transform.Find("Panel");
        panel.gameObject.SetActive(true);

        await Task.Delay(5000);

        panel.gameObject.SetActive(false);
        _credits.transform.Find("TitleText").gameObject.SetActive(true);

        await Task.Delay(5000);

        GameManager.Instance.DataPersistence.DeleteData();
        Application.Quit();
    }
}
