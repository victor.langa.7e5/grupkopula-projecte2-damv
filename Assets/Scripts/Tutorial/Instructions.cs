using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Instructions : MonoBehaviour
{
    [HideInInspector] public bool AllInputReceived;
    [SerializeField] private InputTutorialUI[] _inputTutorialUI;
    [SerializeField] private TextMeshProUGUI _instructionsText;
    [SerializeField] private List<InputTutorialUI.Data> _inputData;
    [SerializeField] private CanvasGroup _canvasGroup;

    private void OnEnable()
    {
        _canvasGroup.alpha = 0;
        StartCoroutine(FadeIn());
    }

    private void OnDisable()
    {
        foreach (var inputTutorial in _inputTutorialUI)
            inputTutorial.gameObject.SetActive(false);
    }

    public IEnumerator WaitForRequiredInputs(string key, ScriptableEvents.ETutorialKeyCodes[] requiredInputs)
    {
        AllInputReceived = false;

        Localization.Instance.GetCurrentLanguageData().TryGetValue(key, out string value);
        _instructionsText.text = value;

        for (int i = 0; i < requiredInputs.Length; i++)
        {
            _inputTutorialUI[i].gameObject.SetActive(true);
            _inputTutorialUI[i].InputReceived = false;
            _inputTutorialUI[i].InputData = _inputData.Find(x => x.RequiredInput.Equals(requiredInputs[i])); ;
            StartCoroutine(_inputTutorialUI[i].WaitForInput());
        }

        while (!_inputTutorialUI[0].InputReceived || !_inputTutorialUI[1].InputReceived || !_inputTutorialUI[2].InputReceived || !_inputTutorialUI[3].InputReceived)
            yield return null;

        yield return new WaitForSeconds(1.5f);

        AllInputReceived = true;
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeIn()
    {
        while (_canvasGroup.alpha < 1)
        {
            _canvasGroup.alpha += Time.deltaTime;
            yield return null;
        }
    }

    private IEnumerator FadeOut()
    {
        while (_canvasGroup.alpha > 0)
        {
            _canvasGroup.alpha -= Time.deltaTime;
            yield return null;
        }

        gameObject.SetActive(false);
    }
}