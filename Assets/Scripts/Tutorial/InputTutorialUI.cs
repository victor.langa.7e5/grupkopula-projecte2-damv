using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class InputTutorialUI : MonoBehaviour
{
    [HideInInspector] public Data InputData;
    [HideInInspector] public bool InputReceived = true;
    [SerializeField] private Image _image;
    [SerializeField] private Color _pressedColor, _nonPressedColor;

    [Serializable]
    public struct Data
    {
        public Sprite InputImage;
        public ScriptableEvents.ETutorialKeyCodes RequiredInput;
    }

    public IEnumerator WaitForInput()
    {
        _image.color = _nonPressedColor;
        _image.sprite = InputData.InputImage;

        while (!Input.GetKeyDown((KeyCode)InputData.RequiredInput))
            yield return null;

        _image.color = _pressedColor;
        InputReceived = true;
    }
}
