using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    // Camera Information
    private Vector3 orignalCameraPos;

    [SerializeField] private float shakeAmount;

    private bool canShake = false;
    private float _shakeTimer;



    // Start is called before the first frame update
    void Start()
    {
        orignalCameraPos = transform.localPosition;
    }

    

    public void ShakeCamera(float duration)
    {
        canShake = true;
        _shakeTimer = 0;
        StartCoroutine(StartCameraShakeEffect(duration));
    }

    IEnumerator StartCameraShakeEffect(float duration)
    {
        while (canShake) {

            if (_shakeTimer < duration)
            {
                transform.localPosition = orignalCameraPos + Random.insideUnitSphere * shakeAmount;
                _shakeTimer += Time.deltaTime;
            }
            else
            {
                _shakeTimer = 0f;
                transform.localPosition = orignalCameraPos;
                canShake = false;
            }
            yield return null;
        }
    }
}
