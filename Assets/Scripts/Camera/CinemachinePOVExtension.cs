using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class CinemachinePOVExtension : CinemachineExtension
{
    [SerializeField] float _cameraSensitivity;
    [SerializeField] float _minMaxAngle = 80f;

    private Vector2 _currentRotation;

    protected override void Awake()
    {
        _currentRotation = transform.localRotation.eulerAngles;
        base.Awake();
    }

    protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        if (vcam.Follow && stage == CinemachineCore.Stage.Aim)
            state.RawOrientation = Quaternion.Euler(-_currentRotation.y, _currentRotation.x, 0f);
            
    }

    //Update the camera rotation values equals tho the mouse movement;
    public void OnLook(InputAction.CallbackContext context)
    {
        _currentRotation += new Vector2(context.ReadValue<Vector2>().x, context.ReadValue<Vector2>().y) * Time.deltaTime * _cameraSensitivity;
        _currentRotation.y = Mathf.Clamp(_currentRotation.y, -_minMaxAngle, _minMaxAngle);
    }
}
