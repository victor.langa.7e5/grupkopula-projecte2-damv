public interface ILasereable
{
    public abstract void OnLaserInteract();
}
