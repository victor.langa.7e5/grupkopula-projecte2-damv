using System.Collections;
using UnityEngine;

public class MineralReflective : MonoBehaviour, ILasereable
{
    [SerializeField] private ToolLaser _laser;
    [SerializeField] private Light _light;
    [SerializeField] private float _maxLightIntensity;
    private float _charge = 0;

    public void OnLaserInteract()
    {
        _light.intensity = _maxLightIntensity;
        _charge = 5;
        if (!_laser.enabled)
        {
            _laser.enabled = true;
            _laser.OnUse();
            StartCoroutine(DisableLaser());
        }
    }

    private IEnumerator DisableLaser()
    {
        _charge -= Time.deltaTime;
        _light.intensity -= _maxLightIntensity / 5 * Time.deltaTime;
        if (_charge > 0)
        {
            yield return null;
            StartCoroutine(DisableLaser());
        }
        else
        { 
            _laser.OnUse();
            _laser.enabled = false;
        }
    }
}
