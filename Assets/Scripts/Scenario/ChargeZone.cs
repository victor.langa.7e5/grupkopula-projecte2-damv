
using UnityEngine;

public class ChargeZone : MonoBehaviour
{
    [SerializeField] private float _chargePerSecond;

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && GameManager.Instance.inventory._currentTool == Tool.EPlayerTool.LANTERN)
            GameManager.Instance.inventory.transform.GetComponentInChildren<ToolLantern>().ChargeLantern(_chargePerSecond * Time.deltaTime);
    }
}
