using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class SeaweedLight : MonoBehaviour, ILasereable
{
    [SerializeField] HDAdditionalLightData _light;
    [SerializeField] SphereCollider _collider;
    bool corrutinaStart;

    [SerializeField] private float _maxLightIntensity;
    [SerializeField] private float _intensityWhenColliderEnable;
    [SerializeField] private float _intensityPerSecond;

    void Awake()
    {
        _light.intensity = 0f;

        _collider.enabled = false;
        corrutinaStart = false;
    }

    private IEnumerator LowerCharge()
    {
        yield return new WaitForSeconds(5f);

        while (_light.intensity > 0)
        {
            _light.intensity -= _intensityPerSecond * Time.deltaTime;

            if (_light.intensity <= _intensityWhenColliderEnable)
            {
                _collider.enabled = false;
            }

            yield return null;
        }

        corrutinaStart = false;
    }

    public void OnLaserInteract()
    {
        _light.intensity += _intensityPerSecond * Time.deltaTime * 2;
        _light.intensity = Mathf.Min(_light.intensity, _maxLightIntensity);

        if (!corrutinaStart)
        {
            corrutinaStart = true;
            StartCoroutine(LowerCharge());
        }

        if (_light.intensity >= _intensityWhenColliderEnable)
            _collider.enabled = true;
    }
}
