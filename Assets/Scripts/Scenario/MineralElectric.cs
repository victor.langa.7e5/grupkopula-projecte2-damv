using UnityEngine;

public class MineralElectric : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            GameManager.Instance.UpdatePointToAim(gameObject.transform);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            GameManager.Instance.UpdatePointToAim(null);
    }
}
