using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public Transform playerTransform;
    public Inventory inventory;
    public DataPersistence DataPersistence;

    public GameObject tutorialManager;
    public GameObject uiCanvas;
    public GameObject limitColliders;
    public GameObject Menu;
    public AudioSource PlayerAudioSource;
    public MusicManager MusicManager;
    public AudioSource MusicAudioSource;

    public bool isTutorialDone = false;

    [HideInInspector] public Transform PointToAim { get; private set; }
    public event Action OnPointToAimUpdated;

    public delegate void ChangePlayerState(Player.EPlayerState playerState);
    public event ChangePlayerState OnChangePlayerStateEvent;

    public void UpdatePointToAim(Transform pointToAim)
    {
        PointToAim = pointToAim;
        if (OnPointToAimUpdated != null)
            OnPointToAimUpdated.Invoke();
    }

    public void ChangePlayerStateEvent(Player.EPlayerState playerState)
    {
        if (OnChangePlayerStateEvent != null)
            OnChangePlayerStateEvent(playerState);
    }

    private void Update()
    {
        //Profundidad provisional
        UIManager.Instance.UpdatePlayerLayout(Tool.EPlayerTool.NONE, null, playerTransform.position.y);
    }

    private void Awake()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;

        LoadData();
    }

    private void Start()
    {
        isTutorialDone = DataPersistence.GameExist;

        if (isTutorialDone)
        {
            PlayerAudioSource.enabled = true;
            inventory.gameObject.SetActive(true);
            var unityEvents = tutorialManager.GetComponent<UnityEvents>();
            unityEvents.SetAnimatorController();
            unityEvents.EnablePlayerCollider();
        }
        else
        {
            MusicManager.enabled = false;
        }
    }

    private void LoadData()
    {
        DataPersistence.LoadData();

        if (DataPersistence.GameExist)
        {
            playerTransform.position = DataPersistence.GameplayData.playerTransformPosition;
            foreach (var seaweedData in DataPersistence.GameplayData.seaweeds)
                seaweedData.SeaweedGameObject.SetActive(seaweedData.IsEnabled);
        }
    }

    public void NewGame() => StartCoroutine(StartNewGame());

    private IEnumerator StartNewGame()
    {
        playerTransform.gameObject.GetComponent<Player>().enabled = false;
        playerTransform.gameObject.GetComponent<PlayerInput>().enabled = false;
        limitColliders.SetActive(false);
        uiCanvas.SetActive(false);

        yield return new WaitForSeconds(3f);

        tutorialManager.SetActive(true);
    }

    public void OnApplicationQuit()
    {
        DataPersistence.SaveAllData();
    }
}
