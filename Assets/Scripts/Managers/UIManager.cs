using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    //Singelton Instance
    private static UIManager _instance;
    public static UIManager Instance { get { return _instance; } }

    private void Start()
    {
        UpdateToolLayout(Tool.EPlayerTool.SONAR);
        UpdateToolDisplay(Tool.EPlayerTool.SONAR);
    }

    [SerializeField] CameraShake _camera;
    [SerializeField] ToolsUI[] _toolsUIs;
    [SerializeField] BeaconToolUI[] _beaconToolUIs;
    [SerializeField] TextMeshProUGUI _dephtText;
    [SerializeField] VisualEffectsUI _playerVisualEffectsUI;

    private void OnEnable() => GameManager.Instance.OnChangePlayerStateEvent += OnChangePlayerState;
    private void OnDisable() => GameManager.Instance.OnChangePlayerStateEvent -= OnChangePlayerState;

    public void UpdatePlayerLayout(Tool.EPlayerTool playerTool, int? currentBeacons = null, float? currentDepht = null)
    {
        if (playerTool != Tool.EPlayerTool.NONE)
        {
            UpdateToolLayout(playerTool);
        }
        else if (currentBeacons != null)
        {
            UpdateBeaconLayout(currentBeacons);
        }
        else
        {
            UpdateDephtLayout(currentDepht);
        }
    }

    private void UpdateToolLayout(Tool.EPlayerTool playerTool)
    {
        foreach (var toolsUI in _toolsUIs)
            toolsUI.ResetState();

        _toolsUIs[(int)playerTool].SetActiveToolState();

        foreach (var toolsUI in _toolsUIs)
            toolsUI.UpdateLayout();
    }

    private void UpdateBeaconLayout(int? currentBeacons)
    {
        foreach (var beaconToolUI in _beaconToolUIs)
            beaconToolUI.ResetState();

        switch (currentBeacons)
        {
            case 0:
                break;
            case 1:
                _beaconToolUIs[0].SetActiveToolState();
                break;
            case 2:
                _beaconToolUIs[0].SetActiveToolState();
                _beaconToolUIs[1].SetActiveToolState();
                break;
            default:
                break;
        }

        foreach (var beaconToolUI in _beaconToolUIs)
            beaconToolUI.UpdateLayout();
    }

    public void UpdateToolDisplay(Tool.EPlayerTool playerTool)
    {
        _toolsUIs[(int)playerTool].UpdateDisplay();
    }

    private void UpdateDephtLayout(float? currentDepht)
    {
        //Provisional
        _dephtText.text = (Mathf.Max(0, -(int)currentDepht)).ToString();

    }

    private void OnChangePlayerState(Player.EPlayerState playerState)
    {
        switch (playerState)
        {
            case Player.EPlayerState.NONE:
                break;
            case Player.EPlayerState.NORMAL:
                _playerVisualEffectsUI.ResetUIEffect();
                break;
            case Player.EPlayerState.DISORIENTED:
                _playerVisualEffectsUI.DisorientedEffect();
                break;
            case Player.EPlayerState.ELECTRIFIED:
                _camera.ShakeCamera(1f);
                //_playerVisualEffectsUI.DisorientedEffect();
                _playerVisualEffectsUI.ShakeUIEffect();
                break;
            default:
                break;
        }
    }

    private void Awake()
    {
        if (_instance != this && _instance != null)
            Destroy(gameObject);
        _instance = this;
    }
}
