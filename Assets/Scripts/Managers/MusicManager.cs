using System.Collections;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _audioClipLevel1;
    [SerializeField] private AudioClip _audioClipLevel2;

    [SerializeField] private float _secondsBetweenSongs;

    private void OnEnable() => StartCoroutine(WaitForPlaySong());

    private IEnumerator WaitForPlaySong()
    {
        yield return new WaitForSeconds(_secondsBetweenSongs);

        if (GameManager.Instance.playerTransform.position.y > -600)
            _audioSource.clip = _audioClipLevel1;
        else
            _audioSource.clip = _audioClipLevel2;

        _audioSource.Play();

        while (_audioSource.isPlaying)
            yield return null;
        
        StartCoroutine(WaitForPlaySong());
    }
}
