using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaweedTutorialManager : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private List<ToolLaser> _lasers = new List<ToolLaser>();
    [SerializeField] private SphereCollider _collider;
    [SerializeField] private int _cycles;
    [SerializeField] private float _seconds;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _collider.enabled = false;
            StartCoroutine(StartTutorial());
        }
    }

    private IEnumerator StartTutorial()
    {
        for (int i = 0; i < _cycles; i++)
        {
            foreach (var laser in _lasers)
            {
                laser.OnUse();
                _audioSource.Stop();
                _audioSource.Play();
            }

            yield return new WaitForSeconds(_seconds);
        }
    }
}
